var gulp = require('gulp');
var importCss = require("gulp-cssimport");
var cleanCss = require('gulp-clean-css');
var importJs =  require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var optionsCss = {};
var optionsRenameCss = {
    suffix: ".min"
};

gulp.task('css', function(){
    return gulp.src('assets/css/app.css')
        .pipe(importCss(optionsCss))
        .pipe(cleanCss())
        .pipe(rename(optionsRenameCss))
        .pipe(gulp.dest('assets/css'))
});

gulp.task('js', function(){
    return gulp.src([
            'assets/plugins/jquery/dist/jquery.min.js',
            'assets/plugins/bootstrap/dist/js/bootstrap.min.js',
            'assets/plugins/slick-carousel/slick/slick.min.js',
            'assets/js/app.js'
        ])
        .pipe(importJs('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js'))
});

gulp.task('default', [ 'css', 'js' ]);