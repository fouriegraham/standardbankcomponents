$(document).ready((e)->

  #######################################################
  #######################################################
  #### INIT ###
  #######################################################
  #######################################################

  #######################################################
  #######################################################
  #### FLOATABLE CONTAINER ###
  #######################################################
  #######################################################
  sbgFloatComponents = $('div[data-floatable-container=true]')
  sbgFloatIndex = 0
  sbgFloatComponents.each((e)->
    sbgFloatIndex++
    componentThis = $(@)
    windowWidth = $(window).width()
    windowOffset = $(window).scrollTop()

    component = {
      this: $(@)
      top: $(@).attr('data-floatable-top')
      parent: $(@).closest('div#floatable-container')
      width: $(@).width()
      height: $(@).height()
      offsetY: $(@).offset().top
      offsetX: $(@).offset().left
    }

    parentContainerHeight = $(component.parent).height()
    component.this.width($(@).closest('div').width())
    $(@).parent('div').addClass('data-floatable-parent-'+sbgFloatIndex)
    componentWidth = $('div.data-floatable-parent-'+sbgFloatIndex).width()

    runFunc = (thisComponent, thisComponentTop, thisComponentWidth)->
      stopX = ($(component.parent).offset().top + parentContainerHeight) - component.height
      if windowWidth >= 992
        component.this.width(thisComponentWidth)
        if $(component.parent).offset().top >= windowOffset
          thisComponent.css('top', 'auto').removeClass('sbg-floating-static').removeClass('sbg-floating-fixed')
        else if stopX - thisComponentTop <= windowOffset
          thisComponent.css('top', stopX - $(component.parent).offset().top).removeClass('sbg-floating-fixed').addClass('sbg-floating-static')
        else
          thisComponent.css('top', thisComponentTop+'px').removeClass('sbg-floating-static').addClass('sbg-floating-fixed')
      else
        component.this.width('auto')
        thisComponent.css('top', 'auto').removeClass('sbg-floating-static').removeClass('sbg-floating-fixed')
      return

    $(window).scroll((e)->
      windowWidth = $(window).width()
      windowOffset = $(window).scrollTop()
      parentContainerHeight = $(component.parent).height()
      runFunc(component.this, component.top, componentWidth)
      return
    )
    $(window).resize((e)->
      windowWidth = $(window).width()
      windowOffset = $(window).scrollTop()
      parentContainerHeight = $(component.parent).height()
      componentWidth = $('div.data-floatable-parent-'+sbgFloatIndex).width()
      runFunc(component.this, component.top, componentWidth)
      return
    )

    runFunc(component.this, component.top, componentWidth)
    return
  )

  #######################################################
  #######################################################
  #### ANCHOR + HEADER SPACE ###
  #######################################################
  #######################################################
  sbgJumptoButtons = $('a[data-jumpto-container]');
  sbgBackToTopButton = $('a.button-backtop')
  sbgJumptoButtons.on('click touch', (e)->
    sbgAnchorWithHeader($(@).attr('data-jumpto-container'))
    return
  )
  sbgBackToTopButton.on('click touch', (e)->
    $('html,body').animate({
      scrollTop: 0
    }, 'fast');
    return
  )
  sbgAnchorWithHeader = (anchor)->
    headerHeight = 50
    if anchor
      if $(anchor).length > 0
        $('html,body').animate({
          scrollTop: $(anchor).offset().top - headerHeight
        }, 'fast');
    else
      if window.location.hash
        hash = window.location.hash.substring(1)
        if hash.indexOf("jumpto_") >= 0
          anchorName = hash.replace("jumpto_", "")
          $('html,body').animate({
              scrollTop: $("#"+anchorName).offset().top - headerHeight
            }, 'fast');
    return
  sbgAnchorWithHeader();

  #######################################################
  #######################################################
  #### CARD SELECTOR ###
  #######################################################
  #######################################################
  sbgCardSelectorButton = $('div.card-filter div.show_label')
  sbgCardSelectorButton.on('click touch', (e)->
    getCardsContainerParent = $(@).parent('div')
    getCardsContainerParent.toggleClass('open')
    return
  )

  #######################################################
  #######################################################
  #### LOAD MORE ###
  #######################################################
  #######################################################
  $('a.sbg-loadmore-button').on('click touch', (e)->
    elementsContainer = $(@).closest('.sbg-loadmore-container')
    elements = elementsContainer.find('div#manage-cards div.load-more-hidden')
    if elements.hasClass('hidden')
      $(@).html('SHOW LESS <i class="fa fa-arrow-up" aria-hidden="true"></i>');
    else
      $(@).html('SHOW MORE <i class="fa fa-arrow-down" aria-hidden="true"></i>');
    elements.toggleClass('hidden')
    return
  )

  #######################################################
  #######################################################
  #### CAROUSELS ###
  #######################################################
  #######################################################
  $('.sbg-benefits-carousel-container').slick({
    dots: false
    infinite: true
    speed: 300
    slidesToShow: 4
    slidesToScroll: 1
    arrows: false
    responsive: [
      {
        breakpoint: 767
        settings: {
          slidesToShow: 1
          slidesToScroll: 1
          infinite: true
          arrows: false
          dots: true
        }
      }
    ]
  });
  $('.card-switch-carousel').slick({
    dots: true
    infinite: true
    speed: 300
    slidesToShow: 1
    slidesToScroll: 1
    arrows: true
    responsive: [
      {
        breakpoint: 767
        settings: {
          slidesToShow: 1
          slidesToScroll: 1
          infinite: true
          arrows: false
          dots: true
        }
      }
    ]
  });
  $('.card-cross-sell-carousel').slick({
    dots: true
    infinite: true
    speed: 300
    slidesToShow: 1
    slidesToScroll: 1
    arrows: false
  });
  
  #######################################################
  #######################################################
  #### TABS ###
  #######################################################
  #######################################################
  $('div.card-tabs').each((e)->
    tabs = $(@)

    ###tabs width###
    tabsWidth = 0
    tabs.find('div.button-container a').each((e)->
      tabsWidth += $(@).width()
      tabsWidth += 50
      return
    ).after((e)->
      tabs.find('div.button-container div.tab-scroller').width(tabsWidth)
      return
    )

    ###click touch###
    tabs.find('div.button-container a').on('click touch', (e)->
      tabs.find('div.button-container a').removeClass('active')
      $(@).addClass('active')
      getIndex = $(@).index()
      tabs.find('div.content-container').removeClass('active')
      tabs.find('div.content-container').eq(getIndex).addClass('active')
      return;
    )
    return
  )
  
  #######################################################
  #######################################################
  #### COMPARE ###
  #######################################################
  #######################################################
  standardBankCompareFunction2.init()

  #######################################################
  #######################################################
  #### WINDOW RESIZE ###
  #######################################################
  #######################################################
  $(window).resize((e)->
    return
  )

  #######################################################
  #######################################################
  #### WINDOW SCROLL###
  #######################################################
  #######################################################
  $(window).scroll((e)->
    windowOffset = $(window).scrollTop()
    windowTop = 200
    if windowOffset > windowTop
      sbgBackToTopButton.css('display','inline-block');
    else
      sbgBackToTopButton.css('display','none');
    return
  )

)

#######################################################
#######################################################
#### COMPARE FUNCTION ###
#######################################################
#######################################################
standardBankCompareFunction2 =
  init: () ->
    this.fetch()
    this.add()
    this.remove()
    this.hideStrip()
    this.comparePage()
    return

  comparePage: ()->
    $('.compare-carousel').slick({
      dots: false
      infinite: false
      speed: 300
      slidesToShow: 3
      slidesToScroll: 1
      arrows: false
      responsive: [
        {
          breakpoint: 767
          settings: {
            slidesToShow: 1
            slidesToScroll: 1
            infinite: false
            arrows: false
            dots: false
          }
        }
      ]
    })
    $('body').on('click touch', 'div.card-compare-add-dropdown a.select', (e)->
      $('div.card-compare-add-dropdown div.dropdown').removeClass('open')
      getParentContainer = $(@).closest('div.card-compare-add-dropdown')
      getChildDropdown = getParentContainer.find('div.dropdown')
      getChildDropdown.toggleClass('open')
      return
    )
    return

  hideStrip: ()->
    $('a.compare-close-button').on('click touch', '', (e)->
      $('div.card-compare-strip').removeClass('visible')
      return
    )
    return

  add: ()->
    $('body').on('click touch', 'a.compare-button, a.compare_page_select_button', (e)->
      getElement = $(@)
      getSection = getElement.attr('data-compare-section')
      getEntry = getElement.attr('data-compare-entry')
      if getElement.hasClass('compare-button')
        getElement.removeClass('compare-button').addClass('compare-button-remove').html('Remove')
      standardBankCompareFunction2.store(getSection, getEntry)
      return
    )
    return

  remove: ()->
    $('body').on('click touch', 'a.compare-button-remove, a.compare-remove-page', (e)->
      getElement = $(@)
      getSection = getElement.attr('data-compare-section')
      getEntry = getElement.attr('data-compare-entry')
      if getElement.hasClass('compare-button-remove')
        getElement.removeClass('compare-button-remove').addClass('compare-button').html('Add to compare')
      standardBankCompareFunction2.delete(getSection, getEntry)
      return
    )
    return

  store: (section, entry)->
    getLocalStorage = localStorage.getItem('sbg_comparison_'+section);
    if getLocalStorage isnt null
      getJSON = JSON.parse(getLocalStorage)
      getEntries = getJSON.entries
      if getEntries.length < 3
        if $.inArray(entry, getEntries) is -1
          getEntries.push(entry)
          updateArray =
            type: section
            entries: getEntries
          localStorage.setItem('sbg_comparison_'+section, JSON.stringify(updateArray))
          standardBankCompareFunction2.updateStrip()
      else
        ###too many entries###
    else
      createArray =
        type: section
        entries: [entry]
      localStorage.setItem('sbg_comparison_'+section, JSON.stringify(createArray))
      standardBankCompareFunction2.updateStrip()
    return

  delete: (section, entry)->
    getLocalStorage = localStorage.getItem('sbg_comparison_'+section);
    if getLocalStorage isnt null
      getJSON = JSON.parse(getLocalStorage)
      getEntries = getJSON.entries
      getEntries.splice($.inArray(entry, getEntries),1);
      updateArray =
        type: section
        entries: getEntries
      localStorage.setItem('sbg_comparison_'+section, JSON.stringify(updateArray))
      standardBankCompareFunction2.updateStrip()
    return

  fetch: ()->
    standardBankCompareFunction2.updateStrip()
    getLocalStorageKeys = Object.keys(localStorage)
    if getLocalStorageKeys.length > 0
      for i in [0...getLocalStorageKeys.length]
        getType = getLocalStorageKeys[i]
        getLocalStorage = localStorage.getItem(getType);
        getJSON = JSON.parse(getLocalStorage)
        getEntries = getJSON.entries
        for i2 in [0...getEntries.length]
          $('a.compare-button[data-compare-entry='+getEntries[i2]+']').removeClass('compare-button').addClass('compare-button-remove').html('Remove')
          $('a.compare_page_select_button[data-compare-entry='+getEntries[i2]+']').addClass('disabled')
    return

  updateStrip: ()->
    getLocalStorageKeys = Object.keys(localStorage)
    if getLocalStorageKeys.length > 0
      for i in [0...getLocalStorageKeys.length]
        getType = getLocalStorageKeys[i]
        getLocalStorage = localStorage.getItem(getType);
        getJSON = JSON.parse(getLocalStorage)
        getSection = getType.replace('sbg_comparison_', '')
        getStrip = $('div.card-compare-strip[data-compare-section='+getSection+']')
        getMessagingComponent = getStrip.find('div.message-component div.messaging')
        getBadgeComponent = getStrip.find('div.message-component div.buttons div.badge')
        getCompareBlocks = $('div.card-compare-block')
        getCompareBlocks.eq(3).html('')
        getCompareBlocks.eq(2).html('')
        getCompareBlocks.eq(1).html('')

        if getJSON.entries.length is 3
          getBadgeComponent.css('display','inline-block')
          getMessagingComponent.html('<p class="color-text-white"><span class="bold">You can now compare all the cards you’ve added</span></p>')
          getBadgeComponent.html(getJSON.entries.length)
          getCompareBlocks.eq(3).html($('div[data-compare-entry='+getJSON.entries[2]+']').html())
          getCompareBlocks.eq(2).html($('div[data-compare-entry='+getJSON.entries[1]+']').html())
          getCompareBlocks.eq(1).html($('div[data-compare-entry='+getJSON.entries[0]+']').html())

        else if getJSON.entries.length is 2
          getBadgeComponent.css('display','inline-block')
          getMessagingComponent.html('<p class="color-text-white"><span class="bold">You can add one other card to compare</span></p>')
          getBadgeComponent.html(getJSON.entries.length)
          getCompareBlocks.eq(3).html($('div#card-compare-add-block').html())
          getCompareBlocks.eq(2).html($('div[data-compare-entry='+getJSON.entries[1]+']').html())
          getCompareBlocks.eq(1).html($('div[data-compare-entry='+getJSON.entries[0]+']').html())

        else if getJSON.entries.length is 1
          getBadgeComponent.css('display','inline-block')
          getMessagingComponent.html('<p class="color-text-white"><span class="bold">You can add another two cards to compare</span></p>')
          getBadgeComponent.html(getJSON.entries.length)
          getCompareBlocks.eq(3).html($('div#card-compare-add-block').html())
          getCompareBlocks.eq(2).html($('div#card-compare-add-block').html())
          getCompareBlocks.eq(1).html('').html($('div[data-compare-entry='+getJSON.entries[0]+']').html())

        else
          getBadgeComponent.css('display','none')
          getMessagingComponent.html('<p class="color-text-white"><span class="bold">You can add up to three cards for comparison</span><br>Select ‘Add to compare’ beneath the card</p>')
          getCompareBlocks.eq(3).html($('div#card-compare-add-block').html())
          getCompareBlocks.eq(2).html($('div#card-compare-add-block').html())
          getCompareBlocks.eq(1).html($('div#card-compare-add-block').html())

        for i2 in [0...getJSON.entries.length]
          $('a.compare_page_select_button[data-compare-entry='+getJSON.entries[i2]+']').addClass('disabled')

        getStrip.addClass('visible')
    return


#######################################################
#######################################################
#### COMPARE FUNCTION ###
#######################################################
#######################################################
